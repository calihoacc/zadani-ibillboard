var app = require('../app');
var request = require('supertest');
var expect = require('chai').expect;
var assert = require('chai').assert;

const redis = require('redis');
const redisDB = redis.createClient();

redisDB.on('connect', function() {
    console.log('Connected to redisDB');
});

describe('GET /', function() {
    it('respond with status 200 OK', function(done) {
        request(app)
        .get('/')
        .set('Accept', 'text/html')
        .expect(200, done)
    });
});

describe('GET /count status', function() {
    it('respond with status 200 OK', function(done) {
        request(app)
        .get('/count')
        .set('Accept', 'text/html')
        .expect(200, done)
    });
});

describe('GET /count value', function() {
    it('respond with integer', function(done) {
        request(app)
        .get('/count')
        .set('Accept', 'text/html')
        .expect(function(res) {
            var isInt = parseInt(res.text) % 1;
            assert.equal('0', isInt, 'is not an integer');
        })
        .expect(200, done)

    });    
});

describe('POST /track', function() {
    it('respond with status 200 OK', function(done) {
        request(app)
        .post('/track')
        .set('Accept', 'json')
        .expect(200, done)
    });
});

describe('File test', function() {
    var jsonfile = require('jsonfile');

    it('write test', function(done) {
        jsonfile.writeFile('./test.json', "Hello", function(err) {
            if(err) {
                throw "Unable to write to file";
            }
            done();
        });
    });

    it('read test', function(done) {
        jsonfile.readFile('./test.json', function(err, data) {
            if(err) {
                throw "Unable to read file";
            }
            done();
        });
    });
});

describe('DB test', function() {

    it('write test', function(done) {
        redisDB.set('test', 'hello', function(err, reply) {
            if(err) {
                throw "Unable to write to db";
            }
            done();
        });
    });

    it('read test', function(done) {
        redisDB.get('test', function(err, reply) {
            if(err) {
                throw "Unable to delete from db";
            }
            done();       
        });
    });

    it('delete test', function(done) {
        redisDB.del('test', function(err, reply) {
            if(err) {
                throw "Unable to delete from db";
            }
            done();      
        });
    });
});