/**
* Zavislosti modulu.
*/

const express = require("express");
const app = express();

const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/**
* Inicializace spojeni s databazi.
*/

const redis = require('redis');
const redisDB = redis.createClient();

redisDB.on('connect', function() {
    console.log('Connected to redisDB');
});


/**
* Pokud v url neni nic zadano, vkladam index.html s formularem
*/

app.get('/', function(req, res) {
    res.sendFile(__dirname + "/index.html");
});


/**
* Na route count vracim hodnotu klice count z redis DB
*/

app.get('/count', function(req, res) {
    redisDB.get('count', function(err, reply) {
        res.end(reply);        
    });
});

/**
* Na route track zpracovavam post data
*/

app.post('/track', function(req, res) {

    var count = parseInt(req.body.count);
    var jsonfile = require('jsonfile');
    var file = 'data.json';

    //pokud byl odeslan formular, uloz json z body do souboru
    if(req.body.submited == 1) {
        jsonfile.writeFile(file, req.body, function(err) {
            if(err) {
                console.error(err);
            } else {
                console.log('Form data was saved into file ' + file);
            }
        });
    }

    //pokud je vyplnena hodnota count, pricti ji k aktualni hodnote z db
    if(count > 0) {
        redisDB.incrby('count', count, function(err, reply) {
            if(err) {
                console.error(err);
            } else {
                console.log('Value of key count in redis DB was incremented by ' + count);
            }
        });
    }

    res.end();
});

module.exports = app;

//tady slibene routovani bez expressu:
var rest = require('./rest_route.js');
Http.createServer(function(request,response){
  var url = request.url;
  switch(true){
    case RegExp('/api/1/servers$').test(url):
      rest.servers(request,response);
      break;
    case RegExp('/api/1/servers/\\d').test(url):
      rest.server(request,response);
      break;
    case RegExp('/api/1/nodes').test(url):
      rest.nodes(request,response);
      break;
    case RegExp('/api/1/url/').test(url):
      rest.urlapi(request,response);
      break;
    default:
      response.end();
  }
}).listen(3000);