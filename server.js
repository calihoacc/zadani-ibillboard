/**
* Inicializace aplikace
*/

var app = require('./app');

app.listen(1337, function() {
    console.log('Started on PORT 1337');
});
